Glenn McGovern is an attorney and motorcyclist who graduated from the University of New Orleans. Since that time, his law firm has focused on helping the victims of civil rights violations, motorcycle accidents, police brutality, employment law and civil rights.

Address: 2637 Edenborn Ave, #101, Metairie, LA 70002

Phone: 504-456-3610